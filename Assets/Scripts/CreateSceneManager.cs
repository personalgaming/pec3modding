﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.IO;
using UnityEngine.EventSystems;

public class CreateSceneManager : MonoBehaviour {

	public InputField numLevel;
	public InputField columns;
	public InputField rows;

	int levelNumber;
	int columnsNumber;
	int rowsNumber;

	public GameObject globalMap;
	public GameObject interactPanel;

	public GameObject blockList;

	public List<Block> blockListb;

	int DEFAULT_ROWS = 7;
	int DEFAULT_COLUMNS = 7;

	int PADDING = 20;

	int SPACES = 2;

	public GameObject levelSavedText;

	[HideInInspector]
	public Block selectedBlock;
	[HideInInspector]
	public GameObject selectedButton;
	[HideInInspector]
	public Block blockPlayer;

	public const String colorUnselected = "#962C34";
	public const String colorSelected = "#36FF50";
	[HideInInspector]
	public bool mouseIsDraged;

	private String path= "Assets/Resources/levels.txt";


	// Use this for initialization
	void Start () {
		columns.text = DEFAULT_COLUMNS+"";
		rows.text = DEFAULT_ROWS+"";
		foreach(Block b in blockList.transform.GetComponentsInChildren<Block>()) {
			blockListb.Add(b);
		}
		selectedBlock = null;
		selectedButton = null;
		mouseIsDraged = false;
		blockPlayer = null;
		levelNumber = 1;
		numLevel.text = levelNumber + "";

		//Create Map default
		CreateMap();
	}

	public void CreateMap() {
		columnsNumber = Int32.Parse(columns.text) + SPACES;
        rowsNumber = Int32.Parse(rows.text) + SPACES;
		levelNumber = Int32.Parse(numLevel.text);

		levelSavedText.SetActive(false);

		Rect rectTrans = globalMap.GetComponent<RectTransform>().rect;
		float width = rectTrans.width;
		float height = rectTrans.height;
		float posX = rectTrans.x;
		float posY = rectTrans.y;

		float cell_width = (width - (float)PADDING * 2f) / columnsNumber;
		float cell_x_origin = (width - (float)PADDING * 2f) / 2f;
		float cell_height = (height - (float)PADDING * 2f) / rowsNumber;
		float cell_y_origin = (height - (float)PADDING * 2f) / 2f;

		float cell_size = cell_width >= cell_height ?  cell_height : cell_width;
		RectTransform interactPanelRect = interactPanel.GetComponent<RectTransform>();
		interactPanelRect.sizeDelta = new Vector2(cell_size, cell_size);
		Debug.Log(cell_size);

		foreach (Transform child in globalMap.transform)
		{
			GameObject.Destroy(child.gameObject);
		}
		for (int i = 0; i < rowsNumber; i++) {
			for (int j = 0; j < columnsNumber; j++) {
				GameObject go = Instantiate(interactPanel, new Vector3(0, 0, 0), Quaternion.identity, globalMap.transform);
				go.transform.localPosition = new Vector3(cell_size * j - cell_x_origin + cell_size / 2, -cell_size * i + cell_y_origin - cell_size / 2, 0);
				go.name = "Cell" + i + "x" + j;
				Block bPlay = go.GetComponent<Block>();
				bPlay.x = i;
				bPlay.y = j;
				Block b = null;
				if (i == 0 && j == 0) b = GetBlockById(BlockList.WALL_TOP_LEFT);
				else if (i == 0 && j == columnsNumber - 1) b = GetBlockById(BlockList.WALL_TOP_RIGHT);					
				else if (i == 0 && j % 3 == 0) b = GetBlockById(BlockList.WALL_TOP_LIGHT);					
				else if (i == 0) b = GetBlockById(BlockList.WALL_TOP);
				else if (i == rowsNumber - 1 && j == 0) b = GetBlockById(BlockList.WALL_BOTTOM_LEFT);
				else if (i == rowsNumber - 1 && j == columnsNumber - 1) b = GetBlockById(BlockList.WALL_BOTTOM_RIGHT);
				else if (i == rowsNumber - 1) b = GetBlockById(BlockList.WALL_BOTTOM);
				else if (j == 0) b = GetBlockById(BlockList.WALL_LEFT);
				else if (j == columnsNumber - 1) b = GetBlockById(BlockList.WALL_RIGHT);
				else {
					b = GetBlockById(BlockList.FLOOR1);
					go.GetComponent<Button>().onClick.AddListener(() => PaintMap(go));
					//go.GetComponent<Button>().onClick.AddListener(delegate { PaintMap(go); });
				}
				go.GetComponentInChildren<Image>().sprite = b.blockImage;
				bPlay.blockImage = b.blockImage;
				bPlay.blockType = b.blockType;
				bPlay.id = b.id;
				bPlay.interactObject = 0;

				//define button interaction

			}
		}
	}

	private Block GetBlockById(int i) {
		foreach(Block b in blockListb) {
			if (b.id == i) return b;
		}
		return null;
	}

	public void SelectedBlock(GameObject but) {
		Color colorS, colorU;
		ColorUtility.TryParseHtmlString(colorSelected, out colorS);
		but.GetComponent<Image>().color = colorS;

		if(selectedButton != null) {
			ColorUtility.TryParseHtmlString(colorUnselected, out colorU);
			selectedButton.GetComponent<Image>().color = colorU;
		}

		selectedBlock = but.GetComponent<AssociatedBlock>().block;
		selectedButton = but;
	}

	public void PaintMap(GameObject panelToPaint) {
		if (panelToPaint != null && selectedBlock != null)
		{
			levelSavedText.SetActive(false);
			Block b = panelToPaint.GetComponent<Block>();
			if(selectedBlock.id == BlockList.PLAYER && b.blockType != Block.BlockType.Block) {
				if(blockPlayer != null) {
					blockPlayer.interactObject = 0;
					blockPlayer.GetComponentsInChildren<Image>()[1].color = new Color(255, 255, 255, 0f);
				}
				panelToPaint.GetComponentsInChildren<Image>()[1].color = new Color(255, 255, 255, 1f);
				panelToPaint.GetComponentsInChildren<Image>()[1].sprite = selectedBlock.blockImage;
				b.interactObject = selectedBlock.id;
				blockPlayer = b;
			}
			else if (selectedBlock.id == BlockList.STATUE && b.blockType != Block.BlockType.Block)
			{
				panelToPaint.GetComponentsInChildren<Image>()[1].color = new Color(255,255,255,1f);
				panelToPaint.GetComponentsInChildren<Image>()[1].sprite = selectedBlock.blockImage;
				b.interactObject = selectedBlock.id;

			}
			else if (!(selectedBlock.id == BlockList.PLAYER || selectedBlock.id == BlockList.STATUE))
			{
				b.blockImage = selectedBlock.blockImage;
				b.blockType = selectedBlock.blockType;
				b.id = selectedBlock.id;
				if (b.interactObject != 0) { 
					b.interactObject = 0;
					panelToPaint.GetComponentsInChildren<Image>()[1].color = new Color(255, 255, 255, 0f);
				}
				panelToPaint.GetComponentsInChildren<Image>()[0].sprite = selectedBlock.blockImage;
			}
		}
	}

	public void OnMouseDown() {
		Debug.Log("MOuse dragged");
		mouseIsDraged = true;
	}

	public void OnMouseUp()
	{
		Debug.Log("Mouse no dragged");
		mouseIsDraged = false;
	}

	public void SaveGame() {
		//the file will be parsed in the follow way:
		// #levelNumber-rows-columns-cell0x0?@interactObject?,cell0x1?@interactObject?....

		String gameLevel = "#"+levelNumber + "-" + rowsNumber + "-" + columnsNumber + "-";
		int i = 0;
		Block[] blocksChild = globalMap.GetComponentsInChildren<Block>();
		foreach(Block b in blocksChild) {
			gameLevel += b.id;
			if(b.interactObject!=0) {
				gameLevel += "@" + b.interactObject;
			}
			if (i < blocksChild.Length-1) //Use count or length as supported by your collection
			{
				i++; 
				gameLevel += ","; 
			}
		}
		String levels = ReadString();
		string[] levelArray = levels.Split('#');
		Boolean levelIn = false;
		String outputString = "";
		foreach (String s in levelArray)
		{
			if (s != ""){
				//access to the first element levelNumber
				int num = Int32.Parse(s.Split('-')[0]);
				if (num > levelNumber && !levelIn)
				{
					outputString += gameLevel + "#" + s;
					levelIn = true;
				}
				else if (num == levelNumber)
				{
					outputString += gameLevel;
					levelIn = true;
				}
				else
				{
					outputString += "#" + s;
				}
			}
		}
		if (!levelIn) outputString += gameLevel;
		outputString.Replace("\n", "");
		WriteString(outputString);
		//Debug.Log(ReadString());
		levelSavedText.SetActive(true);
	}

	public void WriteString(String str) {
		StreamWriter writer = new StreamWriter(path, false);
		writer.WriteLine(str);
		writer.Close();
	}

	public String ReadString() {
		//Read the text from directly from the test.txt file
		StreamReader reader = new StreamReader(path);
		String str = reader.ReadToEnd();
		reader.Close();
		return str;
	}
}
