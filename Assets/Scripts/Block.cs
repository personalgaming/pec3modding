﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Block : MonoBehaviour {

	public enum BlockType {Block, Floor, Player, Box, FloorInteract, Statue}

	public Sprite blockImage;
	public BlockType blockType;
	public int id;
	public int x;
	public int y;
	public int interactObject;

	//only used in statue
	public string name;
	public bool pressingButton;

}
