﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneControl : MonoBehaviour {


	public const int MENU = 0;
	public const int GAME = 1;
	public const int CREATELEVELS = 2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void StartGame() {
		Debug.Log("Start Game!!!!");
		SceneManager.LoadScene(GAME);
	}

	public void CreateLevels() {
		Debug.Log("Create Levels!!!!");
		SceneManager.LoadScene(CREATELEVELS);
	}

	public void ReturnMenu() {
		Debug.Log("Return to menu...");
		SceneManager.LoadScene(MENU);
	}
}
