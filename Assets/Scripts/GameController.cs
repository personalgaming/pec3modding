﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;


public class GameController : MonoBehaviour {

	public int speed = 3;
	float distance = 16f;
	bool isMoving;
	public GameObject link;
	public GameObject map;
	private int direction;
	private float acumTime;
	private float timeToStartWalk = 0.3f;
	private string[] levelsArray;
	private int actualLevel;

	private Vector2 sprite_size;
	private Vector2 origin;
	private Vector2 destination;

	Animator linkAnimator;
	Rigidbody2D linkRigidbody;

	public GameObject blockList;
	private List<Block> blockListb;
	private List<Block> mapList;

	public GameObject blockSample;

	private GameObject statueMoving = null;
	private Rigidbody2D statueRigidbody;
	private Vector2 statueOrigin;
	private Vector2 statueDestination;

	public Text numLevel;

	private string path = "Assets/Resources/levels.txt";

	// Use this for initialization
	void Start () {
		blockListb = new List<Block>();
		mapList = new List<Block>();
		foreach (Block b in blockList.transform.GetComponentsInChildren<Block>())
		{
			blockListb.Add(b);
		}

		direction = 2;
		linkAnimator = link.GetComponent<Animator>();
		linkRigidbody = link.GetComponent<Rigidbody2D>();
		isMoving = false;
		acumTime = 0;
		actualLevel = 10;
		sprite_size = link.GetComponent<SpriteRenderer>().sprite.rect.size;
		Vector2 local_sprite_size = sprite_size / link.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
		Vector3 world_size = local_sprite_size;
		distance = world_size.x;
		ReadLevels();
		CreateMap();

	}
	
	// Update is called once per frame
	void Update () {
		if(!isMoving) {

			if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) && direction != 1) {
				direction = 1;
				acumTime = 0f;
				linkAnimator.SetInteger("Stay", 1);
			}
			else if((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && direction != 2) {
				acumTime = 0f;
				direction = 2;
				linkAnimator.SetInteger("Stay", 2);
			}
			else if ((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) && direction != 3) {
				acumTime = 0f;
				direction = 3;
				linkAnimator.SetInteger("Stay", 3);
			}
			else if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && direction != 4) {
				acumTime = 0f;
				direction = 4;
				linkAnimator.SetInteger("Stay", 4);
			}
			else if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.RightArrow)
			         && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.LeftArrow))
			{
				acumTime = 0;
			}
			else {
				acumTime += Time.deltaTime;
			}

			if(acumTime >= timeToStartWalk) {
				isMoving = true;
				acumTime = 0;
				origin = new Vector2(link.transform.position.x, link.transform.position.y);
				MoveLink();
			}
		}
		else {


		}

	}

	private void FixedUpdate()
	{
		if(isMoving) {
			switch (direction)
			{
				case 1:
					//Debug.Log(destination.y + " " + link.transform.position.y);
					//movement link + statue
					if(statueMoving !=null) {
						if(statueDestination.y <= statueMoving.transform.position.y) {
							statueMoving.transform.position = statueDestination;
							statueRigidbody.velocity = new Vector2(0, 0);
							statueMoving = null;

							link.transform.position = destination;
							origin = destination;

							linkAnimator.SetBool("PushUp", false); 
							linkRigidbody.velocity = new Vector2(0, 0);
							isMoving = false;
							if (LevelFinished()) NextLevel();

						}
						else {
							statueRigidbody.velocity = new Vector2(0, speed);
							linkRigidbody.velocity = new Vector2(0, speed);
						}
					}
					//movement only link
					else if(destination.y <= link.transform.position.y) {
						link.transform.position = destination;
						origin = destination;
						if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
						{
							MoveLink();
						}
						else
						{
							linkAnimator.SetBool("WalkUp", false);
							linkRigidbody.velocity = new Vector2(0, 0);
							isMoving = false;
						}
					} else {
						linkRigidbody.velocity = new Vector2(0, speed);
					}
					break;
				case 2:
					//Debug.Log(destination.y + " " + link.transform.position.y);
					//movement link + statue
					if (statueMoving != null)
					{
						if (statueDestination.x <= statueMoving.transform.position.x)
						{
							statueMoving.transform.position = statueDestination;
							statueRigidbody.velocity = new Vector2(0, 0);
							statueMoving = null;

							link.transform.position = destination;
							origin = destination;

							linkAnimator.SetBool("PushRight", false);
							linkRigidbody.velocity = new Vector2(0, 0);
							isMoving = false;
							if (LevelFinished()) NextLevel();

						}
						else
						{
							statueRigidbody.velocity = new Vector2(speed, 0);
							linkRigidbody.velocity = new Vector2(speed, 0);
						}
					}
					//movement only link
					else if (destination.x <= link.transform.position.x)
					{
						link.transform.position = destination;
						origin = destination;
						if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
						{
							MoveLink();
						}
						else
						{
							linkAnimator.SetBool("WalkRight", false);
							linkRigidbody.velocity = new Vector2(0, 0);
							isMoving = false;
						}
					}
					else
					{
						linkRigidbody.velocity = new Vector2(speed, 0);
					}
					break;
				case 3:
					//Debug.Log(destination.y + " " + link.transform.position.y);
					//movement link + statue
					if (statueMoving != null)
					{
						if (statueDestination.y >= statueMoving.transform.position.y)
						{
							statueMoving.transform.position = statueDestination;
							statueRigidbody.velocity = new Vector2(0, 0);
							statueMoving = null;

							link.transform.position = destination;
							origin = destination;

							linkAnimator.SetBool("PushDown", false);
							linkRigidbody.velocity = new Vector2(0, 0);
							isMoving = false;
							if (LevelFinished()) NextLevel();

						}
						else
						{
							statueRigidbody.velocity = new Vector2(0, -speed);
							linkRigidbody.velocity = new Vector2(0, -speed);
						}
					}
					//movement only link
					else if (destination.y >= link.transform.position.y)
					{
						link.transform.position = destination;
						origin = destination;
						if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
						{
							MoveLink();
						}
						else
						{
							linkAnimator.SetBool("WalkDown", false);
							linkRigidbody.velocity = new Vector2(0, 0);
							isMoving = false;
						}
					}
					else
					{
						linkRigidbody.velocity = new Vector2(0, -speed);
					}
					break;
				case 4:
					//Debug.Log(destination.y + " " + link.transform.position.y);
					//movement link + statue
					if (statueMoving != null)
					{
						if (statueDestination.x >= statueMoving.transform.position.x)
						{
							statueMoving.transform.position = statueDestination;
							statueRigidbody.velocity = new Vector2(0, 0);
							statueMoving = null;

							link.transform.position = destination;
							origin = destination;

							linkAnimator.SetBool("PushLeft", false);
							linkRigidbody.velocity = new Vector2(0, 0);
							isMoving = false;
							if (LevelFinished()) NextLevel();

						}
						else
						{
							statueRigidbody.velocity = new Vector2(-speed, 0);
							linkRigidbody.velocity = new Vector2(-speed, 0);
						}
					}
					//movement only link
					else if (destination.x >= link.transform.position.x)
					{
						link.transform.position = destination;
						origin = destination;
						if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
						{
							MoveLink();
						}
						else
						{
							linkAnimator.SetBool("WalkLeft", false);
							linkRigidbody.velocity = new Vector2(0, 0);
							isMoving = false;
						}
					}
					else
					{
						linkRigidbody.velocity = new Vector2(-speed, 0);
					}
					break;
			}
		}
	}

	public bool MoveStatue(Block dB, int direction) {
		Block bt;
		switch (direction)
		{
			case 1:
				statueOrigin = new Vector2(dB.x, dB.y);
				statueDestination = new Vector2(dB.x, dB.y + distance);
				 bt = GetBlockByPosition((int)statueDestination.x, (int)statueDestination.y);
				if (bt.blockType == Block.BlockType.Floor || bt.blockType == Block.BlockType.FloorInteract) {
					statueMoving = GameObject.Find(dB.name);
					statueRigidbody = statueMoving.GetComponent<Rigidbody2D>();
					//dB.y = dB.y + (int)distance;
					ChangePositionByPositionStatue(dB.x, dB.y, dB.x, dB.y + (int)distance);
					return true;
				}
				else {
					statueMoving = null;
					return false;
				}
			case 2:
				statueOrigin = new Vector2(dB.x, dB.y);
				statueDestination = new Vector2(dB.x + distance, dB.y );
				bt = GetBlockByPosition((int)statueDestination.x, (int)statueDestination.y);
				if (bt.blockType == Block.BlockType.Floor || bt.blockType == Block.BlockType.FloorInteract)
				{
					statueMoving = GameObject.Find(dB.name);
					statueRigidbody = statueMoving.GetComponent<Rigidbody2D>();
					//dB.y = dB.y + (int)distance;
					ChangePositionByPositionStatue(dB.x, dB.y, dB.x + (int)distance, dB.y);
					return true;
				}
				else
				{
					statueMoving = null;
					return false;
				}
			case 3:
				statueOrigin = new Vector2(dB.x, dB.y);
				statueDestination = new Vector2(dB.x, dB.y - distance);
				bt = GetBlockByPosition((int)statueDestination.x, (int)statueDestination.y);
				if (bt.blockType == Block.BlockType.Floor || bt.blockType == Block.BlockType.FloorInteract)
				{
					statueMoving = GameObject.Find(dB.name);
					statueRigidbody = statueMoving.GetComponent<Rigidbody2D>();
					//dB.y = dB.y + (int)distance;
					ChangePositionByPositionStatue(dB.x, dB.y, dB.x, dB.y - (int)distance);
					return true;
				}
				else
				{
					statueMoving = null;
					return false;
				}
			case 4:
				statueOrigin = new Vector2(dB.x, dB.y);
				statueDestination = new Vector2(dB.x - distance, dB.y);
				bt = GetBlockByPosition((int)statueDestination.x, (int)statueDestination.y);
				if (bt.blockType == Block.BlockType.Floor || bt.blockType == Block.BlockType.FloorInteract)
				{
					statueMoving = GameObject.Find(dB.name);
					statueRigidbody = statueMoving.GetComponent<Rigidbody2D>();
					//dB.y = dB.y + (int)distance;
					ChangePositionByPositionStatue(dB.x, dB.y, dB.x - (int)distance, dB.y );
					return true;
				}
				else
				{
					statueMoving = null;
					return false;
				}

		}
		return false;
	}

	private void MoveLink() {
		Block destinationBlock;
		switch (direction) {
			case 1:
				destination = new Vector2(origin.x, origin.y + distance);
				destinationBlock = GetBlockByPosition((int)destination.x, (int)destination.y);
				Debug.Log(destinationBlock.blockType);
				if(destinationBlock.blockType == Block.BlockType.Floor || destinationBlock.blockType == Block.BlockType.FloorInteract) {
					linkAnimator.SetBool("WalkUp", true);
				}
				else if(destinationBlock.blockType == Block.BlockType.Statue) {
					if(MoveStatue(destinationBlock, direction)) {
						linkAnimator.SetBool("PushUp", true); 
					}
					else {
						destination = origin;
					}
				}
				else {
					destination = origin;
				}
				break;
			case 2:
				destination = new Vector2(origin.x + distance, origin.y);
				destinationBlock = GetBlockByPosition((int)destination.x, (int)destination.y);
				if (destinationBlock.blockType == Block.BlockType.Floor || destinationBlock.blockType == Block.BlockType.FloorInteract)
				{
					linkAnimator.SetBool("WalkRight", true);
				}
				else if (destinationBlock.blockType == Block.BlockType.Statue)
				{
					if (MoveStatue(destinationBlock, direction))
					{
						linkAnimator.SetBool("PushRight", true);
					}
					else
					{
						destination = origin;
					}
				}
				else
				{
					destination = origin;
				}
				break;
			case 3:
				destination = new Vector2(origin.x, origin.y - distance);
				destinationBlock = GetBlockByPosition((int)destination.x, (int)destination.y);
				if (destinationBlock.blockType == Block.BlockType.Floor || destinationBlock.blockType == Block.BlockType.FloorInteract)
				{
					linkAnimator.SetBool("WalkDown", true);
				}
				else if (destinationBlock.blockType == Block.BlockType.Statue)
				{
					if (MoveStatue(destinationBlock, direction))
					{
						linkAnimator.SetBool("PushDown", true);
					}
					else
					{
						destination = origin;
					}
				}
				else
				{
					destination = origin;
				}
				break;
			case 4:
				destination = new Vector2(origin.x - distance, origin.y);
				destinationBlock = GetBlockByPosition((int)destination.x, (int)destination.y);
				if (destinationBlock.blockType == Block.BlockType.Floor || destinationBlock.blockType == Block.BlockType.FloorInteract)
				{
					linkAnimator.SetBool("WalkLeft", true);
				}
				else if (destinationBlock.blockType == Block.BlockType.Statue)
				{
					if (MoveStatue(destinationBlock, direction))
					{
						linkAnimator.SetBool("PushLeft", true);
					}
					else
					{
						destination = origin;
					}
				}
				else
				{
					destination = origin;
				}
				break;
		}
	}

	private void ReadLevels() {
		string levels = ReadString();
		levelsArray = levels.Split('#');

	}


	private void CreateMap() {
		mapList = new List<Block>();

		string actualLevelStr = levelsArray[actualLevel];
		while(levelsArray.Length > actualLevel && actualLevelStr=="") {
			actualLevel++;
			actualLevelStr = levelsArray[actualLevel];
		}
		Debug.Log(levelsArray.Length + " " + actualLevel);
		if (levelsArray.Length <= actualLevel) GameObject.Find("SceneControl").GetComponent<SceneControl>().ReturnMenu();
		numLevel.text = "Nº Nivel: " + actualLevel;
		string[] levelProps = actualLevelStr.Split('-');
		int idLevel = Int32.Parse(levelProps[0]);
		int rows = Int32.Parse(levelProps[1]);
		int columns = Int32.Parse(levelProps[2]);
		string[] blocks = levelProps[3].Split(',');
		int x = 0, y = 0;
		int i = 0;
		foreach (Transform child in map.transform)
		{
			GameObject.Destroy(child.gameObject);
		}
		foreach (string s in blocks) {
			int idBlock = 0;
			string[] splits = s.Split('@');

			if (splits.Length > 1)
			{
				idBlock = Int32.Parse(splits[0]);
				int idBlock2 = Int32.Parse(splits[1]);
				if (BlockList.PLAYER == idBlock2)
				{
					link.transform.position = new Vector3(x, -y, 0);
				}
				else
				{
					GameObject go2 = Instantiate(blockSample, new Vector3(0, 0, 0), Quaternion.identity, map.transform);
					go2.transform.localPosition = new Vector3(x, -y, 0);
					Block b2 = GetBlockById(idBlock2);
					go2.GetComponent<SpriteRenderer>().sprite = b2.blockImage;
					go2.GetComponent<SpriteRenderer>().sortingOrder = 2;
					go2.GetComponent<Block>().blockType = b2.blockType;
					go2.GetComponent<Block>().blockImage = b2.blockImage;
					go2.GetComponent<Block>().x = x;
					go2.GetComponent<Block>().y = -y;
					go2.GetComponent<Block>().id = b2.id;
					go2.gameObject.name = "Statue" + i;
					go2.GetComponent<Block>().name = "Statue" + i;
					if (GetBlockById(idBlock).blockType == Block.BlockType.FloorInteract) go2.GetComponent<Block>().pressingButton = true;
					else go2.GetComponent<Block>().pressingButton = false;
					i++;
					go2.GetComponent<Block>().pressingButton = (GetBlockById(idBlock2).blockType == Block.BlockType.FloorInteract);
					mapList.Add(go2.GetComponent<Block>());
				}

			}
            else {
				idBlock = Int32.Parse(s);
			}
			Block b = GetBlockById(idBlock);
			GameObject go = Instantiate(blockSample, new Vector3(0, 0, 0), Quaternion.identity, map.transform);
			go.transform.localPosition = new Vector3(x, -y, 0);
			go.GetComponent<SpriteRenderer>().sprite = b.blockImage;
			go.GetComponent<Block>().blockType = b.blockType;
			go.GetComponent<Block>().blockImage = b.blockImage;
			go.GetComponent<Block>().x = x;
			go.GetComponent<Block>().y = -y;
			go.GetComponent<Block>().id = b.id;
			mapList.Add(go.GetComponent<Block>());

			x++;
			if (x >= columns) {
				y++; x = 0;
			}
			//Debug.Log(s);
		}
	}


	private string ReadString()
	{
		//Read the text from directly from the test.txt file
		StreamReader reader = new StreamReader(path);
		string str = reader.ReadToEnd();
		reader.Close();
		return str;
	}

	private Block GetBlockById(int i)
	{
		foreach (Block b in blockListb)
		{
			if (b.id == i) return b;
		}
		return null;
	}

	private Block GetBlockByPosition(int i, int j) {
		Block retb = null;
		foreach (Block b in mapList)
		{
			//Debug.Log(b.x + " " + b.y + " " +  b.blockType);
			if (b.x == i && b.y == j) {
				if(retb == null || (retb.blockType == Block.BlockType.Floor || retb.blockType == Block.BlockType.FloorInteract)) {
					retb = b;
				}
			}

		}
		return retb;
	}

	private void ChangePositionByPositionStatue(int i1, int j1, int i2, int j2) {
		bool isFloorInteract = false;
		foreach (Block b in mapList)
		{
			//Debug.Log(b.x + " " + b.y + " " +  b.blockType);
			if (b.x == i2 && b.y == j2)
			{
				if (b.blockType == Block.BlockType.FloorInteract)
				{
					isFloorInteract = true;
				}

			}

		}
		foreach (Block b in mapList)
		{
			//Debug.Log(b.x + " " + b.y + " " +  b.blockType);
			if (b.x == i1 && b.y == j1)
			{
				if (b.blockType == Block.BlockType.Statue)
				{
					b.x = i2; b.y = j2;
					b.pressingButton = isFloorInteract;
				}

			}

		}
	}

	private Boolean LevelFinished() {
		bool finished = true;
		foreach (Block b in mapList)
		{
		
			if (b.blockType == Block.BlockType.Statue && !b.pressingButton)
			{
				finished = false;
			}



		}
		return finished;
	}

	private void NextLevel() {
		actualLevel++;
		CreateMap();
		acumTime = 0f;
		direction = 2;

		restarAllAnimations();
	}

	public void RestarLevel() {
		CreateMap();
		acumTime = 0f;
		direction = 2;

		restarAllAnimations();
	}

	private void restarAllAnimations() {
		linkAnimator.SetBool("WalkDown", false);
		linkAnimator.SetBool("WalkUp", false);
		linkAnimator.SetBool("WalkLeft", false);
		linkAnimator.SetBool("WalkRight", false);

		linkAnimator.SetBool("PushDown", false);
		linkAnimator.SetBool("PushUp", false);
		linkAnimator.SetBool("PushLeft", false);
		linkAnimator.SetBool("PushRight", false);
		linkAnimator.SetInteger("Stay", 2);
	}
}
