﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelInteractions : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler {

	CreateSceneManager csm;
	public void Start()
	{
		csm = GameObject.Find("CreateSceneManager").GetComponent<CreateSceneManager>();
	}

	public void OnPointerDown(PointerEventData pointerEventData)
	{
		//Output to console the GameObject's name and the following message
		gameObject.GetComponent<Button>().onClick.Invoke();

	}

	public void OnPointerEnter(PointerEventData pointerEventData)
	{
		//Output to console the GameObject's name and the following message
		if (csm.mouseIsDraged)
		{
			gameObject.GetComponent<Button>().onClick.Invoke();
		}
	}
}
