﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockList : MonoBehaviour {
	public const int FLOOR1 = 1;
	public const int FLOOR2 = 2;
	public const int FLOOR3 = 3;
	public const int FLOOR4 = 4;

	public const int BLOCK1 = 5;
	public const int BLOCK2 = 6;
	public const int BLOCK3 = 7;

	public const int STATUE = 8;
	public const int PLAYER = 9;

	public const int WALL_TOP = 10;
	public const int WALL_RIGHT = 11;
	public const int WALL_BOTTOM = 12;
	public const int WALL_LEFT = 13;

	public const int WALL_TOP_LIGHT = 14;

	public const int WALL_TOP_LEFT = 15;
	public const int WALL_TOP_RIGHT = 16;
	public const int WALL_BOTTOM_RIGHT = 17;
	public const int WALL_BOTTOM_LEFT = 18;

}
