# PEC3Modding

PEC 03 - Modding y Creación de Niveles

Enunciado
En anteriores prácticas hemos analizado videojuegos existentes y hemos aplicado conocimientos de diseño de niveles para la creación de nuevo contenido . Estos análisis y diseños de nivel se han realizado sobre productos ​ya existentes​, pero en la vida real no siempre tendremos un proyecto finalizado desde el cual empezar a trabajar, ya sea porque desarrollemos un producto propio o porque se nos pida realizar un producto o niveles para terceros. En este caso, habitualmente dependerá de nosotros tener un ​framework adecuado​ para poder diseñar, testear e iterar sobre nuestro trabajo, lo que conlleva la creación de unas herramientas y pipelines adecuados.
Aprovechando los conocimientos sobre Unity3D adquiridos en otras asignaturas, y el las explicaciones proporcionadas en el ​Módulo 3​ (PID_00247900), en esta práctica no sólo tendremos que diseñar niveles sino también desarrollar las ​herramientas necesarias​ para realizar nuestro trabajo de la manera ​más eficiente posible​.
Modding y Creación de Niveles · PEC 03 1r Semestre ​pag 1 Estudios de Informática, Multimedia y Telecomunicación
           
   Sokoban ​es un videojuego clásico de puzzles, con incontables iteraciones y versiones a lo largo de la historia del videojuego. En él, controlamos a un personaje que debe empujar cajas y llevarlas hacia posiciones específicas del nivel.
Los objetivos de la práctica son los siguientes:
● Desarrollar un clon de Sokoban en Unity3D u otro motor.
Ya sea en 3D o en 2D, implementar un clon básico de Sokoban con las ​mecánicas básicas​ del juego original: Movimiento de personaje, poder empujar cajas en una cuadrícula, y contar con casillas “objetivo” donde las cajas deben colocarse. El prototipo debe de contar con al menos ​10 niveles​ de dificultad creciente y un botón o tecla para reiniciar el nivel. Superar un nivel cargará el siguiente nivel. El último nivel puede cerrar el juego o llevar al primer nivel. Los assets a utilizar pueden ser primitivas estilo whitebox (cápsulas, cajas, etc.. ), o utilizar assets con licencia Creative Commons.
● Tools de Creación de Niveles
Crear un ​editor de niveles​ o herramienta para crear niveles dentro del propio motor para acelerar la creación e iteración de niveles. Puede crearse un editor de niveles dentro del propio juego, crear una escena específica para el editor de nivel, o programar una extensión de editor que permita la creación y edición de un nivel. La tool debe permitir ​guardar ​y ​cargar ​niveles para su posterior edición, ya sea en un formato de texto (JSON, txt..), o en formato binario (Scriptable Object de Unity3D o formato propio). Los 10 niveles finales deben estar creados y guardados/cargados con esta herramienta.
Para la realización de esta práctica se recomienda utilizar Unity3D, utilizar un array o doble array para estructurar el mapa, utilizar Scriptable Objects para almacenar niveles, e intentar no perder tiempo en la estética o “eye-candy” del prototipo, ya que el objetivo de la práctica radica en las tools e implementación de las herramientas de diseño de nivel.
Se permiten licencias creativas en el diseño final del clon de Sokoban, siempre y cuando mantenga su esencia original (llevar cajas de punto A a punto B teniendo en cuenta el orden y manera de empujarlas).
Modding y Creación de Niveles · PEC 03 1r Semestre ​pag 2 Estudios de Informática, Multimedia y Telecomunicación
          
  La entrega consistirá en:
● Fichero .txt con dirección del repositorio del proyecto completo en GitHub, Bitbucket, GitLab o similar.
Criterios de evaluación
● Implementación con éxito de un clon de Sokoban.
● Diseño de las herramientas y/o editor de niveles, facilidad de uso y UX.
● Capacidad de investigar soluciones técnicas para conseguir los objetivos de la práctica.
● Diseño de los puzzles y progresión entre niveles.
● Se valorarán el diseño e implementación de nuevas mecánicas de juego no presentes en el
original.
Material de referencia
● Temario de la asignatura (PID_00247900).
● Assets de dominio público: ​https://opengameart.org/
● KenneyNL Creative Common Assets: ​https://opengameart.org/users/kenney
● Ejemplo gameplay Sokoban: ​https://www.youtube.com/watch?v=gXE31ZZgJBo